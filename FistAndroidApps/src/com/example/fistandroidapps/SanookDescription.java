package com.example.fistandroidapps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

public class SanookDescription extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_sanook_description);

		Intent i = getIntent();
		String title = i.getStringExtra("title");
		String link = i.getStringExtra("link");

		TextView tvTitle = (TextView) findViewById(R.id.title);
		tvTitle.setText(title);

		WebView webview = (WebView) findViewById(R.id.webview);
		webview.loadUrl(link);
	}
}
