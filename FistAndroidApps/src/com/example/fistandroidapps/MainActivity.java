package com.example.fistandroidapps;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {
	String[] values = new String[] { "Android List View", "Adapter implementation", "Simple List View In Android",
			"Create List View Android", "Android Example", "List View Source Code", "List View Array Adapter",
			"Android Example List View" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	public void summation(View v) {
		TextView result = (TextView) findViewById(R.id.result);
		EditText param1 = (EditText) findViewById(R.id.param1);
		EditText param2 = (EditText) findViewById(R.id.param2);

		String param1Str = param1.getText().toString();
		String param2Str = param2.getText().toString();
		double param1Double = Double.parseDouble(param1Str);
		double param2Double = Double.parseDouble(param2Str);

		double sumResult = param1Double + param2Double;
		result.setText(sumResult + "");
	}

	public void goToListview(View v) {
		Intent i = new Intent(this, ListViewActivity.class);
		startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);
			return rootView;
		}
	}

}
