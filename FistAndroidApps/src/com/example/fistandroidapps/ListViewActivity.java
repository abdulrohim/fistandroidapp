package com.example.fistandroidapps;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListViewActivity extends ListActivity {

	String[] values = new String[] { "111111111", "2222222222", "3333333333", "4444444444", "5555555555" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
		setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		// Toast.makeText(this, values[position], Toast.LENGTH_LONG).show();

		Intent i = new Intent(this, Detail.class);
		i.putExtra("value", values[position]);
		startActivity(i);
	}

}
