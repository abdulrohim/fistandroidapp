package com.example.fistandroidapps;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

public class SanookActivity extends ListActivity {
	ArrayList<String> data = null;
	ArrayList<String> link = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		String url = "http://m.sanook.com/feed/solr/news/latest";
		final AQuery aq = new AQuery(this);
		aq.ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
			@Override
			public void callback(String url, JSONObject json, AjaxStatus status) {
				if (json != null) {
					Toast.makeText(SanookActivity.this, "json ok", Toast.LENGTH_LONG).show();
					Log.d("data", json.toString());
					try {
						JSONArray items = json.getJSONArray("item");
						int itemLength = items.length();
						data = new ArrayList<String>();
						link = new ArrayList<String>();
						for (int i = 0; i < itemLength; ++i) {
							JSONObject item = items.getJSONObject(i);
							data.add(item.getString("title"));
							link.add(item.getString("link"));
						}

						ArrayAdapter<String> adapter = new ArrayAdapter<String>(SanookActivity.this,
								android.R.layout.simple_list_item_1, data);
						setListAdapter(adapter);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Intent i = new Intent(this, SanookDescription.class);
		i.putExtra("title", data.get(position));
		i.putExtra("link", link.get(position));
		startActivity(i);
	}
}
